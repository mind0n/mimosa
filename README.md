# Mimosa

## Get started

Mimosa is a flexible workflow framework.  The usage of the framework is simple & strateforward:
 
```

    // Create state change settings
    var rules = new TriggerStateSettings<char, mState>();

    // Define state change rules
    rules.Setup(mState.New)
        .Allow(mState.Deployed, c=>c.CurtInput == 'y', mAction.CodeReview);
    rules.Setup(mState.Deployed)
        .Allow(mState.Released, c=>c.CurtInput == 'y', mAction.Release);

    // Create trigger with initial state
    var trigger = new Trigger<char, mState>(mState.New, rules);

    // Activate trigger with next appropriate action
    trigger.Next(mAction.Develop);
    trigger.Next(mAction.Approve2);

    // Validate state
    Assert.AreEqual(mState.Released, trigger.State);

    // Define states
    public enum mStates
    {
        New,
        Implemented,
        Approved,
        Rejected,
        Deployed
    }

    // Define actions
    public enum mAction
    {
        Develop,
        Approve,
        Approve2,
        Reject,
        Deploy
    }

```

Recognize the comment content within http request file:

```

            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sampleHttpRequest.http");
            var txt = File.ReadAllText(file);
            var b = new StringBuilder();
            var sm = new Trigger<char,HttpState>(HttpState.Nothing, HttpRequestSyntax.Rules);
            sm.OnTransit += new Trigger<char,HttpState>.OnTransitDelegate((context, newState) =>
            {
                var curtState = context.CurtState;
                if (newState == HttpState.Comment)
                {
                    b.Append(context.CurtInput.ToString());
                }
                return true;
            });

            for (int i = 0; i < txt.Length; i++)
            {
                var ch = txt[i];
                if (ch == '\r' || ch == '\n')
                {
                    sm.Next(TextAction.TakeSymbol, ch);
                }
                else if (char.IsDigit(ch))
                {
                    sm.Next(TextAction.TakeDigit, ch);
                }
                else if (char.IsLetter(ch))
                {
                    sm.Next(TextAction.TakeLetter, ch);
                }
                else if (char.IsWhiteSpace(ch))
                {
                    sm.Next(TextAction.TakeWhitespace, ch);
                }
                else
                {
                    sm.Next(TextAction.TakeSymbol, ch);
                }
            }
            sm.Next(TextAction.TakeEnd, '\0');

            var commentContent = b.ToString();

```