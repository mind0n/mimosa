using System.Text;

namespace Mimosa.Test
{
    [TestClass]
    public class SampleTest
    {
        [TestMethod]
        public void TriggerTS_Success()
        {
            
            var rules = new TriggerStateSettings<char, mState>();
            rules.Setup(mState.New)
                .Allow(mState.Deployed, c=>c.CurtInput == 'y', mAction.CodeReview);
            rules.Setup(mState.Deployed)
                .Allow(mState.Released, c=>c.CurtInput == 'y', mAction.Release);
            
            var trigger = new Trigger<char, mState>(mState.New, rules);
            trigger.Next(mAction.CodeReview, 'y');
            trigger.Next(mAction.Release, 'y');
            

            Assert.AreEqual(mState.Released, trigger.State);
        }

    }
    public enum mAction
    {
        Implement,
        CodeReview,
        Release
    }
    public enum mState
    {
        New,
        Deployed,
        Released,
    }
}
