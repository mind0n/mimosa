using Microsoft.VisualStudio.TestTools.UnitTesting.Logging;
using System.Diagnostics;
using System.Text;

namespace Mimosa.Test
{
    [TestClass]
    public class HttpRequestSyntaxTest
    {
        [TestMethod]
        public void TestHttpRequestSyntax()
        {
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sampleHttpRequest.http");
            var txt = File.ReadAllText(file);
            var b = new StringBuilder();
            var sm = new Trigger<char,HttpState>(HttpState.Nothing, HttpRequestSyntax.Rules);
            sm.OnTransit += new Trigger<char,HttpState>.OnTransitDelegate((context, newState) =>
            {
                var curtState = context.CurtState;
                if (newState == HttpState.Comment)
                {
                    b.Append(context.CurtInput.ToString());
                }
                return true;
            });

            for (int i = 0; i < txt.Length; i++)
            {
                var ch = txt[i];
                if (ch == '\r' || ch == '\n')
                {
                    sm.Next(TextAction.TakeSymbol, ch);
                }
                else if (char.IsDigit(ch))
                {
                    sm.Next(TextAction.TakeDigit, ch);
                }
                else if (char.IsLetter(ch))
                {
                    sm.Next(TextAction.TakeLetter, ch);
                }
                else if (char.IsWhiteSpace(ch))
                {
                    sm.Next(TextAction.TakeWhitespace, ch);
                }
                else
                {
                    sm.Next(TextAction.TakeSymbol, ch);
                }
            }
            sm.Next(TextAction.TakeEnd, '\0');

            var cmt = b.ToString();
            Assert.IsTrue(string.Equals("## Dce iron day futures data", cmt, StringComparison.Ordinal));
        }
        [TestMethod]
        public void TestHttpResponseSyntax()
        {
            var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "sampleHttpResponse.http");
            var txt = File.ReadAllText(file);
            var sm = new Trigger<char, HttpState>(HttpState.Nothing, HttpResponseSyntax.Rules);
            for (int i = 0; i < txt.Length; i++)
            {
                var ch = txt[i];
                if (ch == '\r' || ch == '\n')
                {
                    sm.Next(TextAction.TakeSymbol, ch);
                }
                else if (char.IsDigit(ch))
                {
                    sm.Next(TextAction.TakeDigit, ch);
                }
                else if (char.IsLetter(ch))
                {
                    sm.Next(TextAction.TakeLetter, ch);
                }
                else if (char.IsWhiteSpace(ch))
                {
                    sm.Next(TextAction.TakeWhitespace, ch);
                }
                else
                {
                    sm.Next(TextAction.TakeSymbol, ch);
                }
            }
            sm.Next(TextAction.TakeEnd, '\0');
            var state = sm.Context.CurtState;            
            Assert.AreEqual(HttpState.End, state);
        }

    }
}
