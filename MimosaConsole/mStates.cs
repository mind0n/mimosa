﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MimosaConsole
{
    public enum mState
    {
        New,
        Implemented,
        Approved,
        Rejected,
        Deployed
    }
    public enum mAction
    {
        Develop,
        Approve,
        Approve2,
        Reject,
        Deploy
    }

}
