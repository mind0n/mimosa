﻿using Mimosa;
using System;
using static System.Net.Mime.MediaTypeNames;

namespace MimosaConsole
{
    public class Program
    {

        static void Main(string[] args)
        {
            StartupDemo();

            //var name = "obj.json";
            //var file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, name);
            //var txt = File.ReadAllText(file);

            //ParseJsonT(txt);

            //Console.WriteLine("Press any key to exit ...");
            //Console.ReadKey();
        }

        private static void ParseJsonT(string txt)
        {
            Func<TriggerContext<char>, JState> ScopeState = new Func<TriggerContext<char>, JState>(x => x.Stack.Count < 1 ? JState.Idle : x.Stack.Peek().As<JState>());

            var st = JsonSyntax.Rules;

            var sm = new Trigger<char>(JState.Idle, st);
            sm.Context.Stack.Push(JState.Idle);

            sm.OnTransit += (context, nextState) =>
            {
                var dch = context.CurtInput == '\r' || context.CurtInput == '\t' || context.CurtInput == '\n' ? ' ' : context.CurtInput;
                var scope = ScopeState(context);
                Console.WriteLine($"{dch}: {context.CurtState} -> {nextState} # {scope}");
                return true;
            };

            for (int i = 0; i < txt.Length; i++)
            {
                try
                {
                    var ch = txt[i];

                    if (char.IsDigit(ch))
                    {
                        sm.Next(TextAction.TakeDigit, ch);
                    }
                    else if (char.IsLetter(ch))
                    {
                        sm.Next(TextAction.TakeLetter, ch);
                    }
                    else if (char.IsWhiteSpace(ch))
                    {
                        sm.Next(TextAction.TakeWhitespace, ch);
                    }
                    else
                    {
                        sm.Next(TextAction.TakeSymbol, ch);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.GetBaseException().Message);
                }
            }
        }


        private static void StartupDemo()
        {
            char ch = ' ';
            var trigger = new Trigger(mState.New);
            trigger.Setup(mState.New)
                .Allow(mState.Implemented, null, mAction.Develop);
            trigger.Setup(mState.Implemented)
                .Allow(mState.Approved, () => ch == 'y', mAction.Approve, mAction.Approve2)
                .Allow(mState.Rejected, () => ch != 'y', mAction.Reject);
            trigger.Setup(mState.Approved)
                .Allow(mState.Deployed, null, mAction.Deploy);


            trigger.Activate(mAction.Develop);
            trigger.Activate(mAction.Approve2);
            Console.Write("Approve? (y/n)");
            var rk = Console.ReadKey();
            Console.WriteLine();
            ch = rk.KeyChar;
            if (ch == 'y')
            {
                trigger.Activate(mAction.Approve);
                trigger.Activate(mAction.Deploy);
            }
            else
            {
                trigger.Activate(mAction.Reject);
            }
        }



    }




}