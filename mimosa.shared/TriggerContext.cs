﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mimosa
{
    public class TriggerContext
    {
        public TriggerActions CurtActions { get; } = new TriggerActions();
        public ConcurrentQueue<TriggerActions> ActionHistory { get; } = new ConcurrentQueue<TriggerActions>();
        public string CurtState { get; protected set; }
        public ConcurrentQueue<object> StateHistory { get; } = new ConcurrentQueue<object>();
        public void PushState(object newState)
        {
            if (CurtActions.Count>0)
            {
                ActionHistory.Enqueue(CurtActions);
            }
            CurtActions.Clear();
            if (!string.IsNullOrWhiteSpace(CurtState))
            {
                StateHistory.Enqueue(CurtState);
            }
            CurtState = newState.ToString();
        }
    }

}
