﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mimosa
{
    public class TriggerContext<TInput,TState>
    {
        public TriggerActions CurtActions { get; } = new TriggerActions();
        public TInput CurtInput { get => _curtInput; private set { PrevInput = _curtInput; _curtInput=value; } }
        private TInput _curtInput;
        public TInput? PrevInput { get; private set; }
        public TInput? NextInput { get; private set; }
        public ConcurrentQueue<TriggerActions> ActionHistory { get; } = new ConcurrentQueue<TriggerActions>();
        public TState CurtState { get; protected set; }
        public ConcurrentQueue<object> StateHistory { get; } = new ConcurrentQueue<object>();
        public Stack<object> Stack { get; } = new Stack<object>();
        protected bool isStateSet { get; set; }

        public void PushState(TState newState)
        {
            if (CurtActions.Count>0)
            {
                ActionHistory.Enqueue(CurtActions);
            }
            CurtActions.Clear();
            if (isStateSet)
            {
                StateHistory.Enqueue(CurtState);
                isStateSet = true;
            }
            CurtState = newState;
        }
        public void TakeAction(object action, TInput input, TInput? nextInput = default(TInput?))
        {
            CurtActions.Add(action);
            CurtInput = input;
            NextInput = nextInput;
        }
        public void Reset()
        {
            ActionHistory.Clear();
            CurtActions.Clear();
            StateHistory.Clear();
            Stack.Clear();
            PrevInput = default(TInput?);
            _curtInput = default(TInput);
            NextInput = default(TInput?);
            isStateSet = false;
        }
    }
}
