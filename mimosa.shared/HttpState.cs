﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mimosa
{
    public enum HttpState
    {
        Nothing,
        CommentStart,
        Comment,
        CommentEnding,
        Method,
        BeforeRoute,
        Route,
        AfterRoute,
        Protocol, AfterProtocol,
        BeforeVersion, Version, AfterVersion,
        StatusCode,AfterStatusCode,
        StatusReason,AfterStatusReason,
        BeforeHeaderName, HeaderName, AfterHeaderName,
        BeforeHeaderValue, HeaderValue, AfterHeaderValue,
        BeforeBody, Body,
        ChunkDef,BeforeChunkData,ChunkData,BeforeChunkDef,
        End
    }
}
