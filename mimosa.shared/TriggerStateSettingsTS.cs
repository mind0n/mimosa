﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using static System.Collections.Specialized.BitVector32;

namespace Mimosa
{
    public class TriggerStateSettings<T,Ts> : ConcurrentDictionary<Ts, TriggerState<T, Ts>>
    {
        public string Name { get; init; }
        public TriggerState<T, Ts> Setup(Ts targetState)
        {
            var key = targetState;
            if (this.ContainsKey(key))
            {
                return this[key];
            }
            var nstate = new TriggerState<T, Ts>(targetState);
            this[key] = nstate;
            return nstate;
        }
        public TriggerStates<T, Ts> Setup(params Ts[] targetStates)
        {
            var rlt = new TriggerStates<T, Ts>();
            foreach (var targetState in targetStates)
            {
                var key = targetState;
                if (ContainsKey(key))
                {
                    var it = this[key];
                    rlt.Add(it);
                }
                var nstate = new TriggerState<T, Ts>(targetState);
                this[key] = nstate;
                rlt.Add(nstate);
            }
            return rlt;
        }
    }
    public class TriggerStates<T, Ts> : List<TriggerState<T, Ts>>
    {
        public TriggerStates<T, Ts> Allow(object action, Func<TriggerContext<T, Ts>, bool> checker = null)
        {
            foreach (var i in this)
            {
                i.Allow(action, checker);
            }
            return this;
        }
        public TriggerStates<T, Ts> Allow(Ts state, object action)
        {
            foreach (var i in this)
            {
                i.Allow(state, action);
            }
            return this;
        }
        public TriggerStates<T, Ts> AllowIf(Ts newState, Func<TriggerContext<T, Ts>, bool> checker)
        {
            foreach (var i in this)
            {
                i.AllowIf(newState, checker);
            }
            return this;
        }
        public TriggerStates<T, Ts> Allow(Ts state, Func<TriggerContext<T, Ts>, bool> checker, params object[] actions)
        {
            foreach (var i in this)
            {
                i.Allow(state, checker, actions);
            }
            return this;
        }
    }
}
