﻿using System.Collections.Concurrent;

namespace Mimosa
{
    public class Trigger<T,Ts>
    {
        public delegate bool OnTransitDelegate(TriggerContext<T,Ts> context, Ts nextState);
        public event OnTransitDelegate OnTransit;
        public TriggerContext<T,Ts> Context { get; } = new TriggerContext<T,Ts>();
        public TriggerStateSettings<T,Ts> StateSettings { get; } = new TriggerStateSettings<T,Ts>();
        public Ts State { get{ return Context.CurtState; } }

        public Trigger(Ts initialState, TriggerStateSettings<T,Ts> states = null)
        {
            StateSettings = states ?? new TriggerStateSettings<T,Ts>();
            Context.PushState(initialState);
        }
        public TriggerState<T,Ts> Setup(Ts targetState)
        {
            var rlt = StateSettings.Setup(targetState);
            return rlt;
        }
        public void Next(object action, T input, T? nextInput = default(T?))
        {
            var fromState = StateSettings[Context.CurtState];
            if (fromState != null)
            {
                if (!Context.CurtActions.Contains(action))
                {
                    Context.TakeAction(action, input, nextInput);
                }
                var akey = TriggerState.MakeKey(Context.CurtActions.ToArray());
                if (fromState.Validators.ContainsKey(akey))
                {
                    var vs = fromState.Validators[akey];
                    bool flag = false;
                    foreach (var v in vs)
                    {
                        flag = ValidateInternal(v, input);
                        if (flag)
                        {
                            break;
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception($"{StateSettings.Name}: Action {action} not allowed from state {Context.CurtState}");
                    }
                }
                else
                {
                    bool flag = false;
                    foreach (var vc in fromState.Validators.Values)
                    {
                        foreach(var v in vc)
                        {
                            if (v.Actions.Contains(action))
                            {
                                flag = true;
                                if (v.Actions.Count == 1)
                                {
                                    if (!ValidateInternal(v, input))
                                    {
                                        throw new Exception($"{StateSettings.Name}: Action {action} not verified for state {Context.CurtState}");
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception($"{StateSettings.Name}: Action {action} not defined for state {Context.CurtState}");
                    }
                }
            }
            else
            {
                throw new Exception($"{StateSettings.Name}: Source state not found {Context.CurtState}");
            }
        }

        private bool ValidateInternal(Validator<T,Ts> v, T input)
        {
            var flag = false;
            if (v.Checker == null || v.Checker(Context))
            {
                if (OnTransit == null || OnTransit(Context, v.State))
                {
                    Context.PushState(v.State);
                    flag = true;
                }              
            }
            return flag;
        }
    }
}