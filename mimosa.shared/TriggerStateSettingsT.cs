﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Mimosa
{
    public class TriggerStateSettings<T> : ConcurrentDictionary<string, TriggerState<T>>
    {
        public string Name { get; init; }
        public TriggerState<T> Setup(object targetState)
        {
            var key = targetState.ToString();
            if (this.ContainsKey(key))
            {
                return this[key];
            }
            var nstate = new TriggerState<T>(targetState);
            this[key] = nstate;
            return nstate;
        }
    }
}
