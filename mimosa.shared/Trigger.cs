﻿using System.Collections.Concurrent;

namespace Mimosa
{
    public class Trigger
    {
        public delegate bool OnTransitDelegate(TriggerContext context, object nextState);
        public event OnTransitDelegate OnTransit;
        public TriggerContext Context { get; } = new TriggerContext();
        public TriggerStateSettings States { get; } = new TriggerStateSettings();

        public Trigger(object initialState, TriggerStateSettings states = null)
        {
            States = states ?? new TriggerStateSettings();
            Context.PushState(initialState);
        }
        public TriggerState Setup(object targetState)
        {
            var rlt = States.Setup(targetState);
            return rlt;
        }
        public void Activate(object action)
        {
            var fromState = States[Context.CurtState];
            if (fromState != null)
            {
                if (!Context.CurtActions.Contains(action))
                {
                    Context.CurtActions.Add(action);
                }
                var akey = TriggerState.MakeKey(Context.CurtActions.ToArray());
                if (fromState.Validators.ContainsKey(akey))
                {
                    var vs = fromState.Validators[akey];
                    bool flag = false;
                    foreach (var v in vs)
                    {
                        flag = ValidateInternal(v);
                        if (flag)
                        {
                            break;
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception($"Action {action} not allowed from state {Context.CurtState}");
                    }
                }
                else
                {
                    bool flag = false;
                    foreach (var vc in fromState.Validators.Values)
                    {
                        foreach(var v in vc)
                        {
                            if (v.Actions.Contains(action))
                            {
                                flag = true;
                                if (v.Actions.Count == 1)
                                {
                                    if (!ValidateInternal(v))
                                    {
                                        throw new Exception($"Action {action} not verified for state {Context.CurtState}");
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception($"Action {action} not defined for state {Context.CurtState}");
                    }
                }
            }
            else
            {
                throw new Exception($"Source state not found {Context.CurtState}");
            }
        }

        private bool ValidateInternal(Validator v)
        {
            var flag = false;
            if (v.Checker == null || v.Checker())
            {
                if (OnTransit == null || OnTransit(Context, v.State))
                {
                    Context.PushState(v.State);
                    flag = true;
                }              
            }
            return flag;
        }
    }
}