﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace Mimosa
{
    public class TriggerState
    {
        public ConcurrentDictionary<string, ValidatorCollection> Validators { get; } = new ConcurrentDictionary<string, ValidatorCollection>();
        private object ParentState { get; }
        public TriggerState(object state)
        {
            ParentState = state;
        }
        public TriggerState Allow(object action, Func<bool> checker = null)
        {
            return Allow(ParentState, checker, action);
        }
        public TriggerState Allow(object state, object action)
        {
            return Allow(state, null, action);
        }
        public TriggerState Allow(object state, Func<bool> checker, params object[] actions)
        {
            var key = MakeKey(actions);
            ValidatorCollection vc = null;
            if (Validators.ContainsKey(key))
            {
                vc = Validators[key];
            }
            else
            {
                vc = new ValidatorCollection();
                Validators[key] = vc;
            }
            if (checker != null)
            {
                var validator = new Validator() { Checker = checker, State = state, Actions = new List<object>(actions) };
                vc.Add(validator);
            }
            else
            {
                vc.Add(new Validator() { State = state, Actions = new List<object>(actions) });
            }
            return this;
        }

        public static string MakeKey(object[] actions)
        {
            var acts = actions.OrderBy(x => x.ToString().Trim()).ToList();
            var join = string.Join(",", acts);
            return $"{join}";
        }
    }
    public class Validator
    {
        public Func<bool> Checker { get; set; }
        public object State { get; set; }
        public List<object> Actions { get; set; }
    }
    public class ValidatorCollection : List<Validator>
    {

    }
}
