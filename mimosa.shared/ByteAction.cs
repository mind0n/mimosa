﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mimosa
{
    public enum ByteAction
    {
        TakeLetter,
        TakeDigit,
        TakeWhitespace,
        TakeAny,
        TakeEnd,
    }
}
