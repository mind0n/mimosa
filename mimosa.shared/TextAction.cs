﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Mimosa
{
    public enum TextAction
    {
        TakeLetter,
        TakeSymbol,
        TakeDigit,
        TakeWhitespace,
        TakeEnd,
    }
}
