﻿using mimosa.shared;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace Mimosa
{
    public class TriggerState<T>
    {
        public ConcurrentDictionary<string, ValidatorCollection<T>> Validators { get; } = new ConcurrentDictionary<string, ValidatorCollection<T>>();
        private object ParentState { get; }
        public TriggerState(object state)
        {
            ParentState = state;
        }
        public TriggerState<T> Allow(object action, Func<TriggerContext<T>, bool> checker = null)
        {
            return Allow(ParentState, checker, action);
        }
        public TriggerState<T> Allow(object state, object action)
        {
            return Allow(state, null, action);
        }
        public TriggerState<T> AllowIf(object newState, Func<TriggerContext<T>, bool> checker)
        {
            return Allow(newState, checker, DefaultAction.CheckNextStep);
        }
        public TriggerState<T> Allow(object state, Func<TriggerContext<T>, bool> checker, params object[] actions)
        {
            var key = MakeKey(actions);
            ValidatorCollection<T> vc = null;
            if (Validators.ContainsKey(key))
            {
                vc = Validators[key];
            }
            else
            {
                vc = new ValidatorCollection<T>();
                Validators[key] = vc;
            }
            if (checker != null)
            {
                var validator = new Validator<T>() { Checker = checker, State = state, Actions = new List<object>(actions) };
                vc.Add(validator);
            }
            else
            {
                vc.Add(new Validator<T>() { State = state, Actions = new List<object>(actions) });
            }
            return this;
        }

        public static string MakeKey(object[] actions)
        {
            var acts = actions.OrderBy(x => x.ToString().Trim()).ToList();
            var join = string.Join(",", acts);
            return $"{join}";
        }
    }
    public class Validator<T>
    {
        public Func<TriggerContext<T>, bool> Checker { get; set; }
        public object State { get; set; }
        public List<object> Actions { get; set; }
    }
    public class ValidatorCollection<T> : List<Validator<T>>
    {

    }
}
