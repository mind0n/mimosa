﻿using Mimosa;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mimosa
{
    public class HttpResponseSyntax
    {
        public static TriggerStateSettings<byte, HttpState> Rules { get; private set; }
        static HttpResponseSyntax()
        {
            byte[] httpBytes = new byte[] { 72, 84, 80, 104, 116, 112 };
            Rules = new TriggerStateSettings<byte, HttpState>();
            Rules.Setup(HttpState.Nothing)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '\r' || x.CurtInput == '\n')
                .Allow(HttpState.CommentStart, x => x.CurtInput == '#', ByteAction.TakeAny)
                .Allow(HttpState.Protocol, ByteAction.TakeLetter)
                ;
            Rules.Setup(HttpState.Protocol)
                .Allow(ByteAction.TakeLetter, x => httpBytes.Contains(x.CurtInput))
                .Allow(HttpState.AfterProtocol, ByteAction.TakeWhitespace)
                .Allow(HttpState.BeforeVersion, x => x.CurtInput == '/', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.AfterProtocol)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.BeforeVersion, x => x.CurtInput == '/', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.BeforeVersion)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.Version, ByteAction.TakeDigit)
                ;
            Rules.Setup(HttpState.Version)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '.')
                .Allow(HttpState.AfterVersion, ByteAction.TakeWhitespace)
                .Allow(HttpState.AfterVersion, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.AfterVersion)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.StatusCode, ByteAction.TakeDigit)
                ;
            Rules.Setup(HttpState.StatusCode)
                .Allow(ByteAction.TakeDigit)
                .Allow(HttpState.AfterStatusCode, ByteAction.TakeWhitespace)
                .Allow(HttpState.AfterStatusCode, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.AfterStatusCode)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '\r')
                .Allow(HttpState.StatusReason, ByteAction.TakeLetter)
                .Allow(HttpState.StatusReason, ByteAction.TakeDigit)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.StatusReason)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.AfterStatusReason, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.AfterStatusReason)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.CommentStart)
                .Allow(HttpState.Comment, x => x.CurtInput != '\r' && x.CurtInput != '\n', ByteAction.TakeAny)
                .Allow(HttpState.CommentEnding, x => x.CurtInput == '\r')
                .Allow(HttpState.Nothing, x => x.CurtInput == '\n')
                ;
            Rules.Setup(HttpState.Comment)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny, x => x.CurtInput != '\n' && x.CurtInput != '\r')
                .Allow(HttpState.CommentEnding, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpState.Nothing, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.CommentEnding)
                .Allow(HttpState.Nothing, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;

            Rules.Setup(HttpState.BeforeHeaderName)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.BeforeBody, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpState.Body, x => x.CurtInput == '\n', ByteAction.TakeAny)
                .Allow(HttpState.HeaderName, ByteAction.TakeLetter)
                .Allow(HttpState.HeaderName, ByteAction.TakeDigit)
                .Allow(HttpState.HeaderName, x => x.CurtInput != '\r' && x.CurtInput != '\n', ByteAction.TakeAny)
                .Allow(HttpState.End, ByteAction.TakeEnd)
                ;
            Rules.Setup(HttpState.HeaderName)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny, x => x.CurtInput != '\r' && x.CurtInput != '\n' && x.CurtInput != ':')
                .Allow(HttpState.AfterHeaderName, ByteAction.TakeWhitespace)
                .Allow(HttpState.BeforeHeaderValue, x => x.CurtInput == ':', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.AfterHeaderName)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.BeforeHeaderValue, x => x.CurtInput == ':', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.BeforeHeaderValue)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpState.HeaderValue, ByteAction.TakeLetter)
                .Allow(HttpState.HeaderValue, ByteAction.TakeDigit)
                .Allow(HttpState.HeaderValue, x => x.CurtInput!='\r'&&x.CurtInput!='\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.HeaderValue)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny, x => x.CurtInput!='\r'&&x.CurtInput!='\n')
                .Allow(HttpState.AfterHeaderValue, x => x.CurtInput=='\r', ByteAction.TakeAny)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput=='\n', ByteAction.TakeAny)
                .Allow(HttpState.End, ByteAction.TakeEnd)
                ;
            Rules.Setup(HttpState.AfterHeaderValue)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput=='\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.BeforeBody)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '\r')
                .Allow(HttpState.Body, x => x.CurtInput == '\n', ByteAction.TakeAny)
                .Allow(HttpState.HeaderName, ByteAction.TakeLetter)
                .Allow(HttpState.HeaderName, ByteAction.TakeDigit)
                .Allow(HttpState.HeaderName, x => x.CurtInput != '\r' && x.CurtInput != '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpState.Body)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny)
                .Allow(HttpState.End, ByteAction.TakeEnd)
                ;
        }
    }
}
