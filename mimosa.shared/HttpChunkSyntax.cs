﻿using Mimosa;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mimosa
{
    public class HttpChunkSyntax
    {
        public static TriggerStateSettings<byte, HttpChunkState> Rules { get; private set; }
        static HttpChunkSyntax()
        {
            Rules = new TriggerStateSettings<byte, HttpChunkState>();
            Rules.Setup(HttpChunkState.Nothing)
                .Allow(ByteAction.TakeAny, x=>x.CurtInput == 13 || x.CurtInput == 10)
                .Allow(HttpChunkState.ChunkSize, ByteAction.TakeDigit)
                .Allow(HttpChunkState.ChunkSize, x=>((x.CurtInput>=65 && x.CurtInput<=70) || (x.CurtInput>=97 && x.CurtInput<=102)), ByteAction.TakeLetter)
                ;
            Rules.Setup(HttpChunkState.ChunkSize)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeLetter, x => (x.CurtInput>=65 && x.CurtInput<=70) || (x.CurtInput>=97 && x.CurtInput<=102))
                .Allow(HttpChunkState.BeforeExtName,x => x.CurtInput == ';', ByteAction.TakeAny)
                .Allow(HttpChunkState.BeforeChunkData, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpChunkState.ChunkData, x => x.CurtInput == '\n', ByteAction.TakeAny)
                .Allow(HttpChunkState.End, ByteAction.TakeEnd)
                ;
            Rules.Setup(HttpChunkState.BeforeExtName)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpChunkState.ExtName, ByteAction.TakeLetter)
                ;
            Rules.Setup(HttpChunkState.ExtName)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny, c => c.CurtInput == '-')
                .Allow(ByteAction.TakeAny, c => c.CurtInput == '_')
                .Allow(HttpChunkState.BeforeExtValue, c=>c.CurtInput == '=', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.BeforeExtValue)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpChunkState.BeforeExtName, x=>x.CurtInput == ';', ByteAction.TakeAny)
                .Allow(HttpChunkState.ExtValue, ByteAction.TakeDigit)
                .Allow(HttpChunkState.ExtValue, ByteAction.TakeLetter)
                ;
            Rules.Setup(HttpChunkState.ExtValue)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeAny, c => c.CurtInput != ';' && c.CurtInput != '\r' && c.CurtInput != '\n')
                .Allow(HttpChunkState.BeforeExtName, c=>c.CurtInput == ';', ByteAction.TakeAny)
                .Allow(HttpChunkState.BeforeChunkData, c => c.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpChunkState.ChunkData, c => c.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.BeforeChunkData)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '\r')
                .Allow(HttpChunkState.ChunkData, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.ChunkData)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeAny, x=>x.CurtInput != '\r' && x.CurtInput != '\n')
                .Allow(HttpChunkState.BeforeChunkSize, x => x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpChunkState.ChunkSize, x => x.CurtInput == '\n', ByteAction.TakeAny)
                .Allow(HttpChunkState.End, ByteAction.TakeEnd)
                ;
            Rules.Setup(HttpChunkState.BeforeChunkSize)
                .Allow(HttpChunkState.ChunkSize, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            // Trail part ////////////////////////////////////////////////////
            Rules.Setup(HttpChunkState.BeforeTrail)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '\r')
                .Allow(HttpChunkState.TrailName, ByteAction.TakeDigit)
                .Allow(HttpChunkState.TrailName, ByteAction.TakeLetter)
                .Allow(HttpChunkState.BeforeTrailName, ByteAction.TakeWhitespace)
                .Allow(HttpChunkState.BeforeTrailName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;

            Rules.Setup(HttpChunkState.BeforeTrailName)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpChunkState.End, x=>x.CurtInput == 0, ByteAction.TakeAny)
                .Allow(HttpChunkState.BeforeTrailEnd, x=>x.CurtInput == '\r' || x.CurtInput == '\n', ByteAction.TakeAny)
                .Allow(HttpChunkState.TrailName, ByteAction.TakeLetter)
                .Allow(HttpChunkState.TrailName, ByteAction.TakeDigit)
                .Allow(HttpChunkState.End, ByteAction.TakeEnd)
                ;
            Rules.Setup(HttpChunkState.TrailName)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '_' || x.CurtInput == '-')
                .Allow(HttpChunkState.BeforeTrailValue, x=>x.CurtInput == ':', ByteAction.TakeAny)
                .Allow(HttpChunkState.AfterTrailName, ByteAction.TakeWhitespace)
                ;
            Rules.Setup(HttpChunkState.AfterTrailName)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpChunkState.BeforeTrailValue, x=>x.CurtInput == ':',ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.BeforeTrailValue)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(HttpChunkState.TrailValue, ByteAction.TakeLetter)
                .Allow(HttpChunkState.TrailValue, ByteAction.TakeDigit)
                .Allow(HttpChunkState.AfterTrailValue, x => x.CurtInput == '\r', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.TrailValue)
                .Allow(ByteAction.TakeDigit)
                .Allow(ByteAction.TakeLetter)
                .Allow(ByteAction.TakeWhitespace)
                .Allow(ByteAction.TakeAny, x=>x.CurtInput != '\r' && x.CurtInput != '\n')
                .Allow(HttpChunkState.AfterTrailValue, x=>x.CurtInput == '\r', ByteAction.TakeAny)
                .Allow(HttpChunkState.BeforeTrailName, x => x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.AfterTrailValue)
                .Allow(HttpChunkState.BeforeTrailName, x=>x.CurtInput == '\n', ByteAction.TakeAny)
                ;
            Rules.Setup(HttpChunkState.BeforeTrailEnd)
                .Allow(ByteAction.TakeAny, x => x.CurtInput == '\r' || x.CurtInput == '\n')
                .Allow(HttpChunkState.End, ByteAction.TakeEnd)
                ;
            Rules.Setup(HttpChunkState.End)
                .Allow(ByteAction.TakeEnd)
                .Allow(ByteAction.TakeAny, x=>x.CurtInput == 0)
                ;
        }
    }
    public enum HttpChunkState
    {
        Nothing,
        BeforeChunkSize, ChunkSize,
        BeforeExtName, ExtName, BeforeExtValue, ExtValue,
        BeforeTrail, BeforeTrailName, TrailName, AfterTrailName, BeforeTrailValue, TrailValue, AfterTrailValue, BeforeTrailEnd,
        BeforeChunkData, ChunkData,
        End
    }

}
