﻿using mimosa.shared;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using static System.Collections.Specialized.BitVector32;

namespace Mimosa
{
    public class TriggerState<T, Ts>
    {
        public ConcurrentDictionary<string, ValidatorCollection<T, Ts>> Validators { get; } = new ConcurrentDictionary<string, ValidatorCollection<T, Ts>>();
        private Ts ParentState { get; }
        public TriggerState(Ts state)
        {
            ParentState = state;
        }
        public TriggerState<T, Ts> Allow(object action, Func<TriggerContext<T, Ts>, bool> checker = null)
        {
            return Allow(ParentState, checker, action);
        }
        public TriggerState<T, Ts> Allow(Ts state, object action)
        {
            return Allow(state, null, action);
        }
        public TriggerState<T, Ts> AllowIf(Ts newState, Func<TriggerContext<T, Ts>, bool> checker)
        {
            return Allow(newState, checker, DefaultAction.CheckNextStep);
        }
        public TriggerState<T,Ts> Allow(Ts state, Func<TriggerContext<T,Ts>, bool> checker, params object[] actions)
        {
            var key = MakeKey(actions);
            ValidatorCollection<T, Ts> vc = null;
            if (Validators.ContainsKey(key))
            {
                vc = Validators[key];
            }
            else
            {
                vc = new ValidatorCollection<T,Ts>();
                Validators[key] = vc;
            }
            if (checker != null)
            {
                var validator = new Validator<T,Ts>() { Checker = checker, State = state, Actions = new List<object>(actions) };
                vc.Add(validator);
            }
            else
            {
                vc.Add(new Validator<T,Ts>() { State = state, Actions = new List<object>(actions) });
            }
            return this;
        }

        public static string MakeKey(object[] actions)
        {
            var acts = actions.OrderBy(x => x.ToString().Trim()).ToList();
            var join = string.Join(",", acts);
            return $"{join}";
        }
    }
    public class Validator<T,Ts>
    {
        public Func<TriggerContext<T,Ts>, bool> Checker { get; set; }
        public Ts State { get; set; }
        public List<object> Actions { get; set; }
    }
    public class ValidatorCollection<T,Ts> : List<Validator<T,Ts>>
    {

    }
}
