﻿using Mimosa;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mimosa
{
    public class JsonSyntax
    {
        public static TriggerStateSettings<char> Rules { get; private set; }
        static JsonSyntax()
        {
            Rules = new TriggerStateSettings<char>();
            Rules.Setup(JState.Idle)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.StringStart, x => x.CurtInput == '"', TextAction.TakeSymbol)
                .Allow(JState.Number, TextAction.TakeDigit)
                .Allow(JState.Boolean, IsBool, TextAction.TakeLetter)
                .Allow(JState.Accomplished, TextAction.TakeEnd)
                .Allow(JState.ObjStart, x => { if (x.CurtInput == '{') x.Stack.Push(JState.ObjStart); return x.CurtInput == '{'; }, TextAction.TakeSymbol)
                .Allow(JState.ObjStart, x => { if (x.CurtInput == '[') x.Stack.Push(JState.ArrStart); return x.CurtInput == '['; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.ArrStart)
                .Allow(JState.BeforeArrValue, TextAction.TakeWhitespace)
                .Allow(JState.StringStart, x => { if (x.CurtInput == '"') x.Stack.Push(JState.ArrValueStart); return x.CurtInput == '"'; }, TextAction.TakeSymbol)
                .Allow(JState.Boolean, x => { if (IsBool(x)) { x.Stack.Push(JState.ArrValueStart); return true; } return false; }, TextAction.TakeLetter)
                .Allow(JState.Number, x => { if (x.CurtInput == '-') { x.Stack.Push(JState.ArrValueStart); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.Number, x => { x.Stack.Push(JState.ArrValueStart); return true; }, TextAction.TakeDigit)
                ;
            Rules.Setup(JState.BeforeArrValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.StringStart, x => { if (x.CurtInput == '"') x.Stack.Push(JState.ArrValueStart); return x.CurtInput == '"'; }, TextAction.TakeSymbol)
                .Allow(JState.Boolean, x => { if (IsBool(x)) { x.Stack.Push(JState.ArrValueStart); return true; } return false; }, TextAction.TakeLetter)
                .Allow(JState.Number, x => { if (x.CurtInput == '-') { x.Stack.Push(JState.ArrValueStart); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.Number, x => { x.Stack.Push(JState.ArrValueStart); return true; }, TextAction.TakeDigit)
                ;
            Rules.Setup(JState.ArrValueEnd)
                .Allow(JState.BeforeArrValue, x => x.CurtInput == ',', TextAction.TakeSymbol)
                .Allow(JState.AfterArrValue, TextAction.TakeWhitespace)
                .Allow(JState.ArrEnd, x => { if (x.CurtInput == ']') { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.AfterArrValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.BeforeArrValue, x => x.CurtInput==',', TextAction.TakeSymbol)
                .Allow(JState.ArrEnd, x => { if (x.CurtInput == ']') { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.ArrEnd)
                .Allow(JState.AfterFieldValue, x => ScopeState(x) == JState.ObjStart, TextAction.TakeWhitespace)
                .Allow(JState.AfterArrValue, x => ScopeState(x) == JState.ArrStart, TextAction.TakeWhitespace)
                .Allow(JState.Accomplished, x => ScopeState(x) == JState.Idle, TextAction.TakeWhitespace)
                .Allow(JState.BeforeFieldName, x => x.CurtInput == ',' && ScopeState(x) == JState.ObjStart, TextAction.TakeSymbol)
                .Allow(JState.BeforeArrValue, x => x.CurtInput == ',' && ScopeState(x) == JState.ArrStart, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.ObjStart)
                .Allow(JState.BeforeFieldName, TextAction.TakeWhitespace)
                .Allow(JState.StringStart, x => { if (x.CurtInput == '"') x.Stack.Push(JState.FieldNameStart); return x.CurtInput == '"'; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.BeforeFieldName)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.StringStart, x => { if (x.CurtInput == '"') x.Stack.Push(JState.FieldNameStart); return x.CurtInput == '"'; }, TextAction.TakeSymbol)
                .Allow(JState.ObjEnd, x => { if (ScopeState(x) == JState.ObjStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.FieldNameEnd)
                .Allow(JState.AfterFieldName, TextAction.TakeWhitespace)
                .Allow(JState.BeforeFieldValue, x => x.CurtInput == ':', TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.BeforeFieldValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.Number, x => { if (x.CurtInput == '-') { x.Stack.Push(JState.FieldValueStart); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.Number, x => { x.Stack.Push(JState.FieldValueStart); return true; }, TextAction.TakeDigit)
                .Allow(JState.Boolean, x => { if (IsBool(x)) { x.Stack.Push(JState.FieldValueStart); return true; } return false; }, TextAction.TakeLetter)
                .Allow(JState.StringStart, x => { if (x.CurtInput == '"') { x.Stack.Push(JState.FieldValueStart); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ObjStart, x => { if (x.CurtInput == '{') { x.Stack.Push(JState.ObjStart); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ArrStart, x => { if (x.CurtInput == '[') x.Stack.Push(JState.ArrStart); return x.CurtInput == '['; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.FieldValueEnd)
                .Allow(JState.AfterFieldValue, TextAction.TakeWhitespace)
                .Allow(JState.ObjEnd, x => { if (x.CurtInput == '}' && ScopeState(x) == JState.ObjStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.BeforeFieldName, x => { if (x.CurtInput == ',' && ScopeState(x) == JState.ObjStart) { return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.AfterFieldValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.BeforeFieldName, x => x.CurtInput == ',', TextAction.TakeSymbol)
                .Allow(JState.ObjEnd, x => { if (ScopeState(x) == JState.ObjStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.ObjEnd)
                .Allow(JState.AfterFieldValue, x => ScopeState(x) == JState.ObjStart, TextAction.TakeWhitespace)
                .Allow(JState.AfterArrValue, x => ScopeState(x) == JState.ArrStart, TextAction.TakeWhitespace)
                .Allow(JState.Accomplished, x => ScopeState(x) == JState.Idle, TextAction.TakeWhitespace)
                .Allow(JState.BeforeFieldName, x => x.CurtInput == ',' && ScopeState(x) == JState.ObjStart, TextAction.TakeSymbol)
                .Allow(JState.BeforeArrValue, x => x.CurtInput == ',' && ScopeState(x) == JState.ArrStart, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.StringStart)
                .Allow(JState.String, TextAction.TakeLetter)
                .Allow(JState.String, TextAction.TakeDigit)
                .Allow(JState.String, TextAction.TakeWhitespace)
                .Allow(JState.String, x => x.CurtInput != '"', TextAction.TakeSymbol)
                .Allow(JState.StringEnd, x => IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.Idle, TextAction.TakeSymbol)
                .Allow(JState.FieldNameEnd, x => { if (IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.FieldNameStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.FieldValueEnd, x => { if (IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ArrValueEnd, x => { if (IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.String)
                .Allow(TextAction.TakeLetter)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeSymbol, x => x.CurtInput != '"' || x.PrevInput == '\\')
                .Allow(JState.StringEnd, x => IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.Idle, TextAction.TakeSymbol)
                .Allow(JState.FieldNameEnd, x => { if (IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.FieldNameStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.FieldValueEnd, x => { if (IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ArrValueEnd, x => { if (IsStringEnd(x.CurtInput, x.PrevInput) && ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.StringEnd)
                .Allow(JState.AfterValue, TextAction.TakeWhitespace)
                .Allow(JState.Accomplished, TextAction.TakeEnd)
                ;
            Rules.Setup(JState.Number)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeSymbol, x => IsDigitChar(x.CurtInput, x.PrevInput))
                .Allow(JState.AfterValue, x => ScopeState(x) == JState.Idle, TextAction.TakeWhitespace)
                .Allow(JState.AfterFieldValue, x => { if (ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeWhitespace)
                .Allow(JState.AfterArrValue, x => { if (ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeWhitespace)
                .Allow(JState.ObjEnd, x => { if (x.CurtInput == '}' && ScopeState(x) == JState.ObjStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ArrEnd, x => { if (x.CurtInput == ']' && ScopeState(x) == JState.ArrStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.BeforeArrValue, x => { if (x.CurtInput == ',' && ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.BeforeFieldName, x => { if (x.CurtInput == ',' && ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.Boolean)
                .Allow(TextAction.TakeLetter, IsBool)
                .Allow(JState.AfterValue, x => ScopeState(x) == JState.Idle, TextAction.TakeWhitespace)
                .Allow(JState.AfterFieldValue, x => { if (ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeWhitespace)
                .Allow(JState.AfterArrValue, x => { if (ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeWhitespace)
                .Allow(JState.ObjEnd, x => { if (x.CurtInput == '}' && ScopeState(x) == JState.ObjStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ObjEnd, x => { if (x.CurtInput == '}' && ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ArrEnd, x => { if (x.CurtInput == ']' && ScopeState(x) == JState.ArrStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.ArrEnd, x => { if (x.CurtInput == ']' && ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.BeforeArrValue, x => { if (x.CurtInput == ',' && ScopeState(x) == JState.ArrValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                .Allow(JState.BeforeFieldName, x => { if (x.CurtInput == ',' && ScopeState(x) == JState.FieldValueStart) { x.Stack.Pop(); return true; } return false; }, TextAction.TakeSymbol)
                ;
            Rules.Setup(JState.AfterValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(JState.Accomplished, TextAction.TakeEnd)
                ;
            Rules.Setup(JState.Accomplished)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeEnd);
        }
        private static JState ScopeState(TriggerContext<char> context)
        {
            return context.Stack.Count < 1 ? JState.Idle : context.Stack.Peek().As<JState>();
        }
        private static bool IsBool(TriggerContext<char> context)
        {
            return "true".Contains(context.CurtInput, StringComparison.OrdinalIgnoreCase) || "false".Contains(context.CurtInput, StringComparison.OrdinalIgnoreCase);
        }
        private static bool IsStringEnd(char ch, char? pch)
        {
            return ch == '"' && pch.GetValueOrDefault() != '\\';
        }

        private static bool IsDigitChar(char ch, char? pch)
        {
            return ch == '.' || ch == '-';
        }
    }

    public enum JState
    {
        Idle,
        StringStart, String, StringEnd,
        Number,
        Boolean,
        AfterValue,
        ObjStart, ObjEnd,
        BeforeFieldName,
        FieldNameStart, FieldName, FieldNameEnd,
        AfterFieldName,
        BeforeFieldValue,
        FieldValueStart,
        FieldValueEnd,
        AfterFieldValue,

        ArrStart, ArrEnd,
        BeforeArrValue, ArrValueStart, ArrValue, ArrValueEnd, AfterArrValue,

        Accomplished
    }
}
