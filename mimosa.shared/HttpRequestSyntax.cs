﻿using Mimosa;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Mimosa
{
    public class HttpRequestSyntax
    {
        public static TriggerStateSettings<char, HttpState> Rules { get; private set; }
        static HttpRequestSyntax()
        {
            Rules = new TriggerStateSettings<char, HttpState>();
            Rules.Setup(HttpState.Nothing)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeSymbol, x=>x.CurtInput == '\r' || x.CurtInput == '\n')
                .Allow(HttpState.CommentStart, x=>x.CurtInput == '#', TextAction.TakeSymbol)
                .Allow(HttpState.Method, TextAction.TakeLetter)
                ;
            Rules.Setup(HttpState.CommentStart)
                .Allow(HttpState.Comment, x => x.CurtInput != '\r' && x.CurtInput != '\n', TextAction.TakeSymbol)
                .Allow(HttpState.CommentEnding, x => x.CurtInput == '\r')
                .Allow(HttpState.Nothing, x => x.CurtInput == '\n')
                ;
            Rules.Setup(HttpState.Comment)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeLetter)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeSymbol, x=>x.CurtInput != '\n' && x.CurtInput != '\r')
                .Allow(HttpState.CommentEnding, x => x.CurtInput == '\r', TextAction.TakeSymbol)
                .Allow(HttpState.Nothing, x=>x.CurtInput == '\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.CommentEnding)
                .Allow(HttpState.Nothing, x=>x.CurtInput == '\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.Method)
                .Allow(TextAction.TakeLetter)
                .Allow(HttpState.BeforeRoute, TextAction.TakeWhitespace)
                ;
            Rules.Setup(HttpState.BeforeRoute)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.Route, TextAction.TakeLetter)
                .Allow(HttpState.Route, x => x.CurtInput == '/', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.Route)
                .Allow(TextAction.TakeLetter)
                .Allow(TextAction.TakeSymbol)
                .Allow(TextAction.TakeDigit)
                .Allow(HttpState.AfterRoute, TextAction.TakeWhitespace)
                ;
            Rules.Setup(HttpState.AfterRoute)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.Protocol, x => "http".Contains(x.CurtInput, StringComparison.OrdinalIgnoreCase), TextAction.TakeLetter)
                ;
            Rules.Setup(HttpState.Protocol)
                .Allow(TextAction.TakeLetter, x => "http".Contains(x.CurtInput, StringComparison.OrdinalIgnoreCase))
                .Allow(HttpState.AfterProtocol, TextAction.TakeWhitespace)
                .Allow(HttpState.BeforeVersion, x => x.CurtInput == '/', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.AfterProtocol)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.BeforeVersion, x => x.CurtInput == '/', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.BeforeVersion)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.Version, TextAction.TakeDigit)
                ;
            Rules.Setup(HttpState.Version)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeSymbol, x => x.CurtInput == '.')
                .Allow(HttpState.AfterVersion, TextAction.TakeWhitespace)
                .Allow(HttpState.AfterVersion, x => x.CurtInput == '\r', TextAction.TakeSymbol)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.AfterVersion)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeSymbol, x => x.CurtInput == '\r')
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput == '\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.BeforeHeaderName)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.BeforeBody, x => x.CurtInput == '\r', TextAction.TakeSymbol)
                .Allow(HttpState.Body, x => x.CurtInput == '\n', TextAction.TakeSymbol)
                .Allow(HttpState.HeaderName, TextAction.TakeLetter)
                .Allow(HttpState.HeaderName, TextAction.TakeDigit)
                .Allow(HttpState.HeaderName, x => x.CurtInput != '\r' && x.CurtInput != '\n', TextAction.TakeSymbol)
                .Allow(HttpState.End, TextAction.TakeEnd)
                ;
            Rules.Setup(HttpState.HeaderName)
                .Allow(TextAction.TakeLetter)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeSymbol, x => x.CurtInput != '\r' && x.CurtInput != '\n' && x.CurtInput != ':')
                .Allow(HttpState.AfterHeaderName, TextAction.TakeWhitespace)
                .Allow(HttpState.BeforeHeaderValue, x => x.CurtInput == ':', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.AfterHeaderName)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.BeforeHeaderValue, x => x.CurtInput == ':', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.BeforeHeaderValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(HttpState.HeaderValue, TextAction.TakeLetter)
                .Allow(HttpState.HeaderValue, TextAction.TakeDigit)
                .Allow(HttpState.HeaderValue, x => x.CurtInput!='\r'&&x.CurtInput!='\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.HeaderValue)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeLetter)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeSymbol, x => x.CurtInput!='\r'&&x.CurtInput!='\n')
                .Allow(HttpState.AfterHeaderValue, x => x.CurtInput=='\r', TextAction.TakeSymbol)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput=='\n', TextAction.TakeSymbol)
                .Allow(HttpState.End, TextAction.TakeEnd)
                ;
            Rules.Setup(HttpState.AfterHeaderValue)
                .Allow(HttpState.BeforeHeaderName, x => x.CurtInput=='\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.BeforeBody)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeSymbol, x => x.CurtInput == '\r')
                .Allow(HttpState.Body, x => x.CurtInput == '\n', TextAction.TakeSymbol)
                .Allow(HttpState.HeaderName, TextAction.TakeLetter)
                .Allow(HttpState.HeaderName, TextAction.TakeDigit)
                .Allow(HttpState.HeaderName, x => x.CurtInput != '\r' && x.CurtInput != '\n', TextAction.TakeSymbol)
                ;
            Rules.Setup(HttpState.Body)
                .Allow(TextAction.TakeWhitespace)
                .Allow(TextAction.TakeLetter)
                .Allow(TextAction.TakeDigit)
                .Allow(TextAction.TakeSymbol)
                .Allow(HttpState.End, TextAction.TakeEnd)
                ;
        }
    }
}
