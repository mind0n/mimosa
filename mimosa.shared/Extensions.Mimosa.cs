﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Mimosa
{
    public static class Extensions
    {
        public static T As<T>(this object inputObject)
        {
            try
            {
                if (inputObject == null)
                {
                    return default(T);
                }
                var inputType = inputObject.GetType();
                var targetType = typeof(T);
                if (targetType.IsAssignableFrom(inputType))
                {
                    return (T)inputObject;
                }
                else
                {
                    var rlt = Convert.ChangeType(inputObject, targetType);
                    return (T)rlt;
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.GetBaseException().ToString());
                return default(T);
            }
        }

    }
}
