﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Mimosa
{
    public class TriggerStateSettings : ConcurrentDictionary<string, TriggerState>
    {
        public TriggerState Setup(object targetState)
        {
            var key = targetState.ToString();
            if (this.ContainsKey(key))
            {
                return this[key];
            }
            var nstate = new TriggerState(targetState);
            this[key] = nstate;
            return nstate;
        }
    }
}
