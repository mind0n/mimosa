﻿using System.Collections.Concurrent;

namespace Mimosa
{
    public class Trigger<T>
    {
        public delegate bool OnTransitDelegate(TriggerContext<T> context, object nextState);
        public event OnTransitDelegate OnTransit;
        public TriggerContext<T> Context { get; } = new TriggerContext<T>();
        public TriggerStateSettings<T> States { get; } = new TriggerStateSettings<T>();

        public Trigger(object initialState, TriggerStateSettings<T> states = null)
        {
            States = states ?? new TriggerStateSettings<T>();
            Context.PushState(initialState);
        }
        public TriggerState<T> Setup(object targetState)
        {
            var rlt = States.Setup(targetState);
            return rlt;
        }

        public void Next(object action, T input, T? nextInput = default(T?))
        {
            var fromState = States[Context.CurtState];
            if (fromState != null)
            {
                if (!Context.CurtActions.Contains(action))
                {
                    Context.TakeAction(action, input, nextInput);
                }
                var akey = TriggerState.MakeKey(Context.CurtActions.ToArray());
                if (fromState.Validators.ContainsKey(akey))
                {
                    var vs = fromState.Validators[akey];
                    bool flag = false;
                    foreach (var v in vs)
                    {
                        flag = ValidateInternal(v, input);
                        if (flag)
                        {
                            break;
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception($"{States.Name}: Action {action} not allowed from state {Context.CurtState}");
                    }
                }
                else
                {
                    bool flag = false;
                    foreach (var vc in fromState.Validators.Values)
                    {
                        foreach(var v in vc)
                        {
                            if (v.Actions.Contains(action))
                            {
                                flag = true;
                                if (v.Actions.Count == 1)
                                {
                                    if (!ValidateInternal(v, input))
                                    {
                                        throw new Exception($"{States.Name}: Action {action} not verified for state {Context.CurtState}");
                                    }
                                }
                                break;
                            }
                        }
                    }
                    if (!flag)
                    {
                        throw new Exception($"{States.Name}: Action {action} not defined for state {Context.CurtState}");
                    }
                }
            }
            else
            {
                throw new Exception($"{States.Name}: Source state not found {Context.CurtState}");
            }
        }

        private bool ValidateInternal(Validator<T> v, T input)
        {
            var flag = false;
            if (v.Checker == null || v.Checker(Context))
            {
                if (OnTransit == null || OnTransit(Context, v.State))
                {
                    Context.PushState(v.State);
                    flag = true;
                }              
            }
            return flag;
        }
    }
}